import React, { useState, useEffect } from "react";

import "./App.scss";
import logo from "./assets/img/logo.svg";
import Converter from "./components/Converter/Converter";
import Loader from "./shared/components/Loader/Loader";
import Offline from './shared/components/errors/Offline';

function App() {
  const [showLoader, setLoaderVisibility] = useState(true);
  const [isOnline, setOnline] = useState(navigator.onLine);

  /**
   * Bind document online and offline events.
   */
  useEffect(() => {
    window.addEventListener("online", () => setOnline(true));
    window.addEventListener("offline", () => setOnline(false));

    return () => {
      window.addEventListener("online", () => setOnline(true));
      window.addEventListener("offline", () => setOnline(false));
    }
  }, []);

  /**
   * Loads converter app if user is online
   */
  const loadConverter = () => {
    return (
      <React.Fragment>
        <Converter setLoaderVisibility={setLoaderVisibility} />
        {showLoader ? <Loader /> : ""}
      </React.Fragment>
    );
  }

  return (
    <div className="app container pb-5">
      <div className="row">
        <header className="app-header mb-3">
          <nav className="navbar navbar-expand-lg navbar-light py-2">
            <a className="navbar-brand" href="/">
              <img className="logo" src={logo} alt="Revoult" />
            </a>
          </nav>
        </header>
      </div>
      {isOnline ? loadConverter() : <Offline />}
    </div>
  );
}

export default App;
