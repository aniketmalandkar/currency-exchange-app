import React, { useState, useRef, useEffect, forwardRef, useImperativeHandle } from 'react';

import { DropdownTypes } from '../../shared/constants';
import './DropdownWithText.scss';
import './flags.scss';

const DropdownWithText = (props, ref) => {
  const [currencyOpen, setCurrencyOpen] = useState(false);
  const [searchValue, setSearchValue] = useState("");

  const inputRef = useRef(null);
  const searchRef = useRef(null);
  const dropdownRef = useRef(null);

  const CurrencyCodes = props.currencyCodes;
  const balance = CurrencyCodes[props.currency].balance;

  useImperativeHandle(ref, () => ({
    focus: () => inputRef.current.focus()
  }));

  /**
   * Bind document click to close dropdown.
   */
  useEffect(() => {
    document.addEventListener("mousedown", onDocumentClick);

    return () => document.removeEventListener("mousedown", onDocumentClick);
  }, []);

  /**
   * Close drodown on document click except click of dropdown area.
   * @param {Event} evt Click event object
   */
  const onDocumentClick = (evt) => {
    if (dropdownRef.current.contains(evt.target)) {
      return false;
    }

    setCurrencyOpen(false);
  }

  /**
   * Change the value of currency of dropdown select click
   * @param {string} code 
   */
  const onDropdownSelect = (code) => {
    setCurrencyOpen(false);
    setSearchValue("");
    props.onCurrencyChange(code, props.type);
    inputRef.current.focus();
  }

  /**
   * Opens currency dropdown and set focus in search textbox
   */
  const onDropdownOpen = () => {
    setCurrencyOpen(!currencyOpen);
    setTimeout(() => searchRef.current.focus(), 300);
  }

  /**
   * Renders currency list
   */
  const getCurrencyList = () => {
    const currencyList = [];

    for (const code in CurrencyCodes) {
      const srhVal = searchValue.trim().toLowerCase();

      if (!srhVal.trim() || (code.toLowerCase().includes(srhVal) || CurrencyCodes[code].name.toLowerCase().includes(srhVal))) {
        currencyList.push(<li key={`${code}-${props.type}`} className={`dropdown-item py-3 ${props.currency === code ? 'selected' : ''}`} onClick={() => onDropdownSelect(code)}>
          <i className={`d-inline-block mr-2 icon-${code}`}></i>
          {code}
          <small className="text-muted ml-2">{CurrencyCodes[code].name}</small>
        </li>
        )
      }
    }
    return currencyList;
  }

  return (
    <div className="form-group form-group-lg">
      <label htmlFor={`from-data-${props.type}`} className="d-flex justify-content-between"><span>You {props.type}</span><span className={(balance < props.currencyVal && props.type === DropdownTypes.SEND) ? "text-danger" : ""}>{`Balance: ${CurrencyCodes[props.currency].symbol}${balance}`}</span></label>

      <div className="input-group input-group-lg">
        <input id={`from-data-${props.type}`} type="text" className="form-control" ref={inputRef} value={props.currencyVal} onChange={(e) => props.onTextChange(e.target.value, props.type)} />

        <span className="input-group-btn input-group-append" ref={dropdownRef}>
          <button id={`cur-btn-${props.type}`} className="btn btn-primary dropdown-toggle btn-lg" type="button" data-toggle="dropdown" onClick={onDropdownOpen}>{props.currency}</button>
          <ul className={`dropdown-menu w-100 px-2 ${currencyOpen ? 'show' : ''}`}>
            <li className="dropdown-item search"><input type="text" ref={searchRef} value={searchValue} className="form-control d-inline-block border-0" placeholder="Search Currency" onChange={(e) => setSearchValue(e.target.value)} /></li>
            <div className="dropdown-divider"></div>
            {getCurrencyList()}
          </ul>
        </span>

      </div>
    </div>
  );
}

export default forwardRef(DropdownWithText);