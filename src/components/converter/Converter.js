import React, { useEffect, useRef, useState } from "react";
import Chart from "react-google-charts";

import swapSymbol from "../../assets/img/swap.svg";
import tradeSymbol from "../../assets/img/trade.svg";
import CurrencyCodes from "../../shared/mocks/pockets.json";
import ExchangeRatesApi from "../../services/ExchangeRatesApi";
import { ChartPeriod, DropdownTypes } from "../../shared/constants";
import DropdownWithText from "../DropdownWithText/DropdownWithText";
import "./Converter.scss";

function Converter(props) {
  const [appState, fetchRates, fetchRatesOfPeriod] = ExchangeRatesApi();

  const [sendCurrency, setSendCurrency] = useState("GBP");
  const [receiveCurrency, setReceiveCurrency] = useState("USD");

  const [sendValue, setSendValue] = useState("");
  const [receiveValue, setReceiveValue] = useState("");

  const sendTextInputRef = useRef(null);
  const receiveTextInputRef = useRef(null);

  /**
   * fetch new rates after interval of 10s.
   */
  useEffect(() => {
    const interval = setInterval(() => fetchRates(sendCurrency), 10000);

    fetchRates(sendCurrency);
    fetchRatesOfPeriod(sendCurrency, ChartPeriod.MONTH);

    return () => clearInterval(interval);
  }, [fetchRates, fetchRatesOfPeriod, sendCurrency, props]);

  /**
   * Update value of when new rates are fetched or receive currency is changed
   */
  useEffect(() => {
    props.setLoaderVisibility(false);
    updateValues();
  }, [appState.rates, receiveCurrency]);

  /**
   * Calls when value of sendTextInputRef is changed
   * Sets focus to Send textbox onload
   */
  useEffect(() => sendTextInputRef.current.focus(), [sendTextInputRef]);

  /**
   * Function calls when currency is changed
   * @param {string} newCurrency New selected currency
   * @param {DropdownTypes} type type Defines which type of currency changed
   */
  const onCurrencyChange = (newCurrency, type) => {
    if (type === DropdownTypes.SEND) {
      if (newCurrency === receiveCurrency) setReceiveCurrency(sendCurrency);
      setSendCurrency(newCurrency);
    } else {
      if (newCurrency === sendCurrency) setSendCurrency(receiveCurrency);
      setReceiveCurrency(newCurrency);
    }
  };

  /**
   * Function calls on textbox value change of send or recieve
   * @param {string} val Value of textbox
   * @param {DropdownTypes} type Defines which type of textbox value changed
   */
  const onValueChangeText = (val, type) => {
    const regexp = /^(?:\d*\.(?!00)\d{0,2}|\d+)$/;

    if (val === "" || val === ".") {
      setSendValue(val);
      setReceiveValue(val);
    } else if (regexp.test(val)) {
      if (type === DropdownTypes.SEND) {
        setSendValue(val);
        setReceiveValue(parseFloat((val * appState.rates[receiveCurrency]).toFixed(2)));
      } else {
        setReceiveValue(val);
        setSendValue(parseFloat((val / appState.rates[receiveCurrency]).toFixed(2)));
      }
    }
  };

  /**
   * Updates value of textbox when any of currency type is changed
   */
  const updateValues = () => {
    if (appState.rates && sendValue && sendValue !== ".") {
      setReceiveValue(parseFloat((sendValue * appState.rates[receiveCurrency]).toFixed(2)));
    }
  };

  /**
   * Swaps currency and fetch new currency exchange data as per send currency type.
   */
  const onSwapBtnClick = () => {
    const sendCur = sendCurrency;

    setSendCurrency(receiveCurrency);
    setReceiveCurrency(sendCur);
    fetchRates(sendCurrency);

    sendTextInputRef.current.focus();
  };

  /**
   * Swaps pocket balance after validation
   */
  const onExchangeClick = () => {
    // prettier-ignore
    if (!sendValue || !parseFloat(sendValue) || CurrencyCodes[sendCurrency].balance < parseFloat(sendValue)) {
      return;
    }

    // prettier-ignore
    CurrencyCodes[sendCurrency].balance = parseFloat((CurrencyCodes[sendCurrency].balance - sendValue).toFixed(2));
    // prettier-ignore
    CurrencyCodes[receiveCurrency].balance = parseFloat((CurrencyCodes[receiveCurrency].balance + receiveValue).toFixed(2));

    setSendValue("");
    setReceiveValue("");

    sendTextInputRef.current.focus();
  };

  const loadChart = () => {
    const periodRates = appState.periodRates;
    const chartData = [[sendCurrency, receiveCurrency]];
    const chartOptions = { hAxis: { title: sendCurrency }, vAxis: { title: receiveCurrency } };

    for (var date in periodRates) {
      chartData.push([new Date(date), periodRates[date][receiveCurrency]]);
    }

    chartData.sort((a, b) => b[0] - a[0]);

    return (
      <Chart
        chartType="LineChart"
        loader={<div>Loading Chart..</div>}
        data={chartData}
        options={chartOptions}
      />
    );
  };

  return (
    <div className="converter-container row">
      <div className="col-12 mb-5 text-center">
        <h1 className="display-5 mt-5 mb-4 mx-2 font-weight-bold">A Radically Better Account</h1>

        <p className="text-muted  mx-2">
          Imagine managing your entire financial life from a single app. That's our goal at Revolut. From everyday spending and budgeting, to foreign currency exchange and easy-access stock trading. We're here to help you get more from your money.
        </p>
      </div>

      <div className="col-12 col-lg pt-3">
        <div className="row">
          <div className="col-sm-12 col-md-6 col-lg-5">
            <DropdownWithText
              type={DropdownTypes.SEND}
              currency={sendCurrency}
              currencyVal={sendValue}
              onTextChange={onValueChangeText}
              onCurrencyChange={onCurrencyChange}
              currencyCodes={CurrencyCodes}
              ref={sendTextInputRef}
            />

            <div className="text-right my-4">
              <span className="py-2 px-3 mr-4 text-primary font-weight-light">
                <img className="rate-logo mr-2" src={tradeSymbol} alt="Revoult" />
                <small>{sendCurrency}</small> 1 = <small>{receiveCurrency}</small>{" "} {appState.rates ? appState.rates[receiveCurrency].toFixed(2) : "1"}
              </span>

              <span className="py-1 px-2 rounded border border-primary shadow-sm swap-btn" onClick={onSwapBtnClick}>
                <img className="rate-logo" src={swapSymbol} alt="Revoult" />{" "}
              </span>
            </div>

            <DropdownWithText
              type={DropdownTypes.RECEIVE}
              currency={receiveCurrency}
              currencyVal={receiveValue}
              onTextChange={onValueChangeText}
              onCurrencyChange={onCurrencyChange}
              currencyCodes={CurrencyCodes}
              ref={receiveTextInputRef}
            />

            <button
              className={`btn btn-primary btn-block btn-lg mt-5`}
              disabled={!sendValue || !parseFloat(sendValue) || CurrencyCodes[sendCurrency].balance < sendValue ? true : false}
              onClick={onExchangeClick}>
              Exchange
          </button>
          </div>

          <div className="col-sm-12 col-md-6 col-lg-7 pl-4">
            {appState.periodRates ? loadChart() : "Loading chart..."}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Converter;
