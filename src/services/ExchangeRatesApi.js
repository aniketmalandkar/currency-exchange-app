import { useReducer, useCallback } from 'react';
import reducer, { initialState } from '../reducer';
import { Actions, ChartPeriod } from '../shared/constants';


const ExchangeRatesApi = () => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const fetchRates = useCallback(async (currency) => {
        fetch(`https://api.exchangeratesapi.io/latest?base=${currency}`)
            .then(response => response.json())
            .then(data => {
                dispatch({ type: Actions.CHANGE_RATES, data });
            });
    }, []);

    const fetchRatesOfPeriod = useCallback(async (currency, period) => {
        const date = new Date();
        const toDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

        switch (period) {
            case ChartPeriod.MONTH:
                date.setMonth(date.getMonth() - 1);
                break;
            default:
                date.setDate(date.getDate() - 1);
        }

        const fromDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

        fetch(`https://api.exchangeratesapi.io/history?start_at=${fromDate}&end_at=${toDate}&base=${currency}`)
            .then(response => response.json())
            .then(data => {
                dispatch({ type: Actions.RATES_OF_PERIOD, data });
            });
    }, []);

    return [state, fetchRates, fetchRatesOfPeriod];
}

export default ExchangeRatesApi;