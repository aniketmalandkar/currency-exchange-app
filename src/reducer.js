import { Actions } from './shared/constants';

export const initialState = {
    rates: null,
    periodRates: null
};

const reducer = (state = initialState, { type, data } = {}) => {
    switch (type) {
        case Actions.CHANGE_RATES:
            return { ...state, ...data };
        case Actions.RATES_OF_PERIOD:
            return { ...state, periodRates: data.rates };
        default:
            return state;
    }
};

export default reducer;