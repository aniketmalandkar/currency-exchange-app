export const Actions = {
    CHANGE_RATES: "CHANGE_RATES",
    RATES_OF_PERIOD: "RATES_OF_PERIOD"
}

export const DropdownTypes = {
    SEND: "send",
    RECEIVE: "receive"
}

export const ChartPeriod = {
    DAY: 'DAY',
    WEEK: 'WEEK',
    MONTH: 'month',
    YEAR: 'year',
}