import React from 'react';

const Offline = () => {
    return (
        <React.Fragment>
            <h1 className="text-center">Offline</h1>
            <p className="text-center">Please check your internet connection and refresh a page.</p>
        </React.Fragment>
    )
}

export default Offline;